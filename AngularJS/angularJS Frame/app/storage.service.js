/**
 * Created by hienpt on 1/12/17.
 */
altairApp
    .service('storage', [
        '$http',
        function($http) {
            var vm = this;

            /**
             * Set an object to local storage
             * @param key
             * @param data
             */
            vm.set = function(key, data) {
                localStorage.setItem(key,JSON.stringify(data));
            };

            /**
             * Get an object from localstorage by key
             * @param key
             */
            vm.get = function(key) {
                return JSON.parse(localStorage.getItem(key));
            };

            vm.setToken = function(token) {
                localStorage.setItem("USER-TOKEN",token);
            };

            vm.getToken = function() {
                return localStorage.getItem("USER-TOKEN");
            };

            vm.setUserInfo = function(userInfo) {
                localStorage.setItem("USER-INFO",JSON.stringify(userInfo));
            };

            vm.getUserInfo = function() {
                return JSON.parse(localStorage.getItem("USER-INFO"));
            };

            vm.delToken  = function() {
                localStorage.removeItem("USER-TOKEN");
            };

            vm.clear = function() {
                localStorage.clear();
            };
         }
    ])
;