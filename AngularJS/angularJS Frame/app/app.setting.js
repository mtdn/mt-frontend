/**
 * Created by hienpt on 1/12/17.
 */
(function () {
    'use strict';
    angular.module('altairApp')
        .service('setting', setting);

    setting.$inject = ['storage'];

    function setting(storage) {
        var vm = this;

        vm.captchaSiteKey = "6Lc3MRoUAAAAAFvvFXeS-Fxssg35_dewAm-GMFmO";

        //vm.api_url = 'https://192.168.0.33:8443/';
        //vm.api_url = 'https://10.61.117.237:8443/';
        //var base_url = "https://10.61.117.237:8888";
        var base_url = "https://192.168.0.33:8888";
        vm.api_url = {
            login: 'https://192.168.0.33:8888/identity-api/authentication'
        };

        //vm.menu = [
        //    {
        //        id: 0,
        //        title: 'Dashboard',
        //        icon: '&#xE871;',
        //        link: 'restricted.dashboard',
        //        roles: ['pcim_admin', 'pcim_staff']
        //    },
        //    {
        //        id: 1,
        //        title: 'Potential customers',
        //        icon: '&#xE0AF;',
        //        link: 'restricted.companies',
        //        roles: ['pcim_admin', 'pcim_staff']
        //    },
        //    {
        //        id: 2,
        //        title: 'Contacts/Leads',
        //        icon: '&#xE03F;',
        //        link: 'restricted.contacts',
        //        roles: ['pcim_admin', 'pcim_staff']
        //    },
        //    {
        //        id: 3,
        //        title: 'Settings',
        //        icon: '&#xE8B8;',
        //        link: 'restricted.settings',
        //        roles: ['pcim_admin']
        //    }
        //    ,
        //    {
        //        id: 4,
        //        title: 'CIM',
        //        icon: '&#xE871;',
        //        link: 'restricted.settings',
        //        roles: ['pcim_admin']
        //    }
        //];

        vm.menu = [
            {
                id: 0,
                title: 'CIM',
                icon: 'cim',
                submenu: [
                    {
                        title: 'Customer Information',
                        link: 'restricted.cim'
                    }
                ],
                roles: ['pcim_admin']
            },
            {
                id: 1,
                title: 'SAM',
                icon: '&#xE871;',
                submenu: [
                    {
                        title: 'Sale Activity Management',
                        link: 'restricted.sale_activity_management'
                    }
                ],
                roles: ['pcim_admin']
            },
            {
                id: 2,
                title: 'SSM',
                icon: '&#xE871;',
                submenu: [
                    {
                        title: 'Price Policy Management',
                        link: 'restricted.price_policy_management'
                    },
                    {
                        title: 'Quotation Management',
                        link: 'restricted.quotation_management'
                    },
                    {
                        title: 'Proposal Management',
                        link: 'restricted.proposal_management'
                    },
                    {
                        title: 'Template Management',
                        link: 'restricted.template_management'
                    }
                ],
                roles: ['pcim_admin']
            },
            {
                id: 3,
                title: 'CCM',
                icon: '&#xE871;',
                submenu: [
                    {
                        title: 'Customer Care Activity',
                        link: 'restricted.customer_care_activity'
                    },
                    {
                        title: 'Present List',
                        link: 'restricted.present_list'
                    }
                ],
                roles: ['pcim_admin']
            },
            {
                id: 4,
                title: 'DBR',
                icon: '&#xE871;',
                submenu: [
                    {
                        title: 'Home Page',
                        link: 'restricted.dbrHomePage'
                    },
                    {
                        title: 'Report',
                        link: 'restricted.dbrReport'
                    }
                ],
                roles: ['pcim_admin']
            }
        ];
        vm.languages = [
            {id: 1, title: 'English', value: 'gb'},
            {id: 2, title: 'Vietnamese', value: 'vn'},
            {id: 3, title: 'Myanmar', value: 'mm'}
            //{id: 1, title: 'English', value: 'gb'},
            //{id: 2, title: 'French', value: 'fr'},
            //{id: 3, title: 'Chinese', value: 'cn'},
            //{id: 4, title: 'Dutch', value: 'nl'},
            //{id: 5, title: 'Italian', value: 'it'},
            //{id: 6, title: 'Spanish', value: 'es'},
            //{id: 7, title: 'German', value: 'de'},
            //{id: 8, title: 'Polish', value: 'pl'}
        ];

        /**
         * Get allowed roles
         * @param stateName
         */
        vm.getAllowRoles = function (stateName) {
            var roles = undefined;
            for (var i = 0; i < vm.menu.length; i++) {
                var menuItem = vm.menu[i];
                if (menuItem.link == stateName) {
                    roles = menuItem.roles;
                    break;
                } else {
                    if (menuItem.submenu) {
                        for (var j = 0; j < menuItem.submenu.length; j++) {
                            var subMenuItem = menuItem.submenu[j];
                            if (subMenuItem && (subMenuItem.link == stateName)) {
                                roles = subMenuItem.roles;
                                break;
                            }
                        }
                    }
                }
            }

            if (roles) {
                console.log("Requires roles: " + roles);
            } else {
                console.log("This state do not require any special role");
            }
            return roles;
        };

        /**
         * Get current user roles
         * @returns {Array}
         */
        vm.getCurrentUserRoles = function () {
            var roles = [];
            if (storage.getUserInfo()) {
                var tmpUserRoles = storage.getUserInfo().roles;
                //console.log("user roles:" + tmpUserRoles);
                for (var i = 0; i < tmpUserRoles.length; i++) {
                    roles.push(tmpUserRoles[i].code);
                }
            }

            return roles;
        };

        vm.ROLES = {
            ADMIN: "admin",
            NORMAL: "normal",
            SUPPLIER: "supplier",
            PROCUREMENT_STAFF: "procurement_staff",
            PROCUREMENT_MANAGER: "procurement_manager",
            CEO: "ceo"
        };
    }
})();







