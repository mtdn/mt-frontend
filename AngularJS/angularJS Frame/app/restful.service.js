altairApp
    .service('restful', [
        '$http','storage','routing',
        function($http,storage,routing) {
            var vm = this;

            vm.get = function(url, data, onSuccess, onError) {
                $http(
                    {
                        method: 'GET',
                        url: url,
                        data: data
                    }
                ).then(function (response) {
                        console.log("on success");
                        onSuccess(response);
                    })
                    .catch(function (errorResponse) {
                        console.log("on error");
                        onError (errorResponse);
                    });
            };

            vm.post = function(url, headers, data, onSuccess, onError) {
                $http(
                    {
                        method: 'POST',
                        url: url,
                        headers: headers,
                        data: data
                    }
                ).then(function (response) {
                        console.log("on success");
                        onSuccess(response);
                    })
                    .catch(function (errorResponse) {
                        console.log("on error");
                        onError(errorResponse);
                    });
            };

            vm.getWithTokenHeader = function(url, headers, data, onSuccess, onError) {
                var headers_with_token = headers;
                headers_with_token.token = storage.getToken();

                $http(
                    {
                        method: 'GET',
                        url: url,
                        headers: headers_with_token,
                        data: data
                    }
                ).then(function (response) {
                        console.log("on success");
                        onSuccess(response);
                    })
                    .catch(function (errorResponse) {
                        console.log("on error");
                        if (errorResponse.status=='403') {
                            storage.clear();
                            routing.goToRfpsPage();
                        } else {
                            onError(errorResponse);
                        }
                    });
            };

            vm.postWithTokenHeader = function(url, headers, data, onSuccess, onError) {
                var headers_with_token = headers;
                headers_with_token.token = storage.getToken();

                $http(
                    {
                        method: 'POST',
                        url: url,
                        headers: headers_with_token,
                        data: data
                    }
                ).then(function (response) {
                        console.log("on success");
                        onSuccess(response);
                    })
                    .catch(function (errorResponse) {
                        console.log("on error");
                        if (errorResponse.status=='403') {
                            storage.clear();
                            routing.goToRfpsPage();
                        } else {
                            onError(errorResponse);
                        }
                    });
            };

            vm.putWithTokenHeader = function(url, headers, data, onSuccess, onError) {
                var headers_with_token = headers;
                headers_with_token.token = storage.getToken();

                $http(
                    {
                        method: 'PUT',
                        url: url,
                        headers: headers_with_token,
                        data: data
                    }
                ).then(function (response) {
                        console.log("on success");
                        onSuccess(response);
                    })
                    .catch(function (errorResponse) {
                        console.log("on error");
                        if (errorResponse.status=='403') {
                            storage.clear();
                            routing.goToRfpsPage();
                        } else {
                            onError(errorResponse);
                        }
                    });
            };

            vm.deleteWithTokenHeader = function(url, headers, data, onSuccess, onError) {
                var headers_with_token = headers;
                headers_with_token.token = storage.getToken();
                $http(
                    {
                        method: 'DELETE',
                        url: url,
                        headers: headers_with_token,
                        data: data
                    }
                ).then(function (response) {
                        console.log("on success");
                        onSuccess(response);
                    })
                    .catch(function (errorResponse) {
                        console.log("on error");
                        if (errorResponse.status=='403') {
                            storage.clear();
                            routing.goToRfpsPage();
                        } else {
                            onError(errorResponse);
                        }
                    });
            }
        }
    ])
;