'use strict';
(function () {

	function initializeKeycloak() {
		console.log("... initializeKeycloak....");
		var keycloakConfig = {
		    "realm": "master",
		    "url": "http://localhost:8080/auth",
		    "ssl-required": "external",
		    "clientId": "MyTel",
		    "credentials": {
		        "secret": "" +
                "6e9dcaa2-91e9-4fe4-8ca0-ec29e715f599"
		    },
		    "enable-cors": true,
		    "confidential-port": 0

		};
	  var keycloak = Keycloak(keycloakConfig);
	  keycloak.init({
	    onLoad: 'login-required',
	    checkLoginIframe:false
	  }).success(function () {
	  	console.log('... ini keycloak ..');
	    keycloak.loadUserInfo().success(function (userInfo) {
	    	console.log(".. user info.."+userInfo);
	      bootstrapAngular(keycloak, userInfo);
	    });
	  });
	  console.log("... end initializeKeycloak ...");
	}

  function bootstrapAngular(keycloak, userInfo) {
  	console.log('...bootstrapAngular....');
  	//add here
    altairApp.run(function ($rootScope, $http, $interval, $cookies) {
    	console.log('..bootstrapAngular run block..');
				var updateTokenInterval = $interval(function () {
          // refresh token if it's valid for less then 15 minutes
          keycloak.updateToken(15)
            .success(function (refreshed) {
              if (refreshed) {
                $cookies.put('X-Authorization-Token', keycloak.token);
              }
            });
        }, 10000);

        $cookies.put('X-Authorization-Token', keycloak.token);

        $rootScope.userLogout = function () {
          $cookies.remove('X-Authorization-Token');
					$interval.cancel(updateTokenInterval);
          keycloak.logout();
        };

				$rootScope.authData = {};
        $rootScope.authData.username = userInfo.preferred_username;
        $rootScope.authData.name = userInfo.preferred_username;
        $rootScope.authData.id = userInfo.sub;
        //    $rootScope.authData.email = userInfo.email;
        $rootScope.authData.token = keycloak.token;

        console.log("../keycloak" + JSON.stringify(keycloak));
        console.log("../userInfo" + JSON.stringify(userInfo));
        $rootScope.authData.role = userInfo.user_realm_role;
        console.log("../userRole" + $rootScope.authData.role);
				// $http.jsonp("http://localhost:9000/test?callback=JSON_CALLBACK")
				// 	.success(function (response) {
				// 		$rootScope.authData.token = response.token;
				// 		$rootScope.authData.username = response.username;
				// 	});
				console.log(keycloak.token);
      });
      	
    	angular.bootstrap(document, ['altairApp']);
  }

	initializeKeycloak();
}());

