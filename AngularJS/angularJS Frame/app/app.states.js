altairApp
    .config([
        '$stateProvider',
        '$urlRouterProvider',
        function ($stateProvider, $urlRouterProvider) {
            /**
             * When invalid url --> redirect to /
             */
            $urlRouterProvider.otherwise('/');

            /**
             * Routing base on state
             */
            $stateProvider
                // -- RESTRICTED --
                .state("restricted", {
                    abstract: true,
                    url: "",
                    templateUrl: 'app/views/restricted.html',
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'lazy_uikit',
                                'lazy_selectizeJS',
                                'lazy_switchery',
                                'lazy_prismJS',
                                'lazy_autosize',
                                'lazy_iCheck',
                                'lazy_themes'
                            ]);
                        }]
                    }
                })

                // 1. DASHBOARD
                // .state("restricted.dashboard", {
                //     url: "/",
                //     templateUrl: 'app/features/dashboard/dashboardView.html',
                //     controller: 'dashboardCtrl',
                //     resolve: {
                //         deps: [
                //             '$ocLazyLoad', function ($ocLazyLoad) {
                //                 return $ocLazyLoad.load([
                //                     'lazy_charts_c3',
                //                     'lazy_google_maps',
                //                     'app/features/dashboard/dashboardController.js'
                //                 ], {serie: true});
                //             }
                //         ]
                //     },
                //     data: {
                //         pageTitle: 'Dashboard'
                //     }
                // })
                // 2. COMPANIES
                .state("restricted.companies", {
                    url: "/companies",
                    templateUrl: 'app/features/companies/companiesView.html',
                    controller: 'companiesCtrl',
                    resolve: {
                        deps: [
                            '$ocLazyLoad', function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    'app/features/companies/companiesController.js'
                                ], {serie: true});
                            }
                        ]
                    },
                    data: {
                        pageTitle: 'Companies'
                    }
                })
                // 3. CONTACTS
                .state("restricted.contacts", {
                    url: "/contacts",
                    templateUrl: 'app/features/contacts/contactsView.html',
                    controller: 'contactsCtrl',
                    resolve: {
                        deps: [
                            '$ocLazyLoad', function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    'app/features/contacts/contactsController.js'
                                ], {serie: true});
                            }
                        ]
                    },
                    data: {
                        pageTitle: 'Contacts'
                    }
                })
                // 4. SETTINGS
                .state("restricted.settings", {
                    url: "/settings",
                    templateUrl: 'app/features/settings/settingsView.html',
                    controller: 'settingsCtrl',
                    resolve: {
                        deps: [
                            '$ocLazyLoad', function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    'app/features/settings/settingsController.js'
                                ], {serie: true});
                            }
                        ]
                    },
                    data: {
                        pageTitle: 'Settings'
                    }
                })
                // 5. Example
                .state("restricted.example", {
                    url: "/example",
                    templateUrl: 'app/features/pages/blog_listView.html',
                    data: {
                        pageTitle: 'Example'
                    }
                })

                 //1. CIM
                .state("restricted.cim", {
                    url: "/",
                    templateUrl: 'app/features/mytel/cim/cimView.html',
                    controller: 'cimCtrl',
                    resolve: {
                        deps: [
                            '$ocLazyLoad', function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    'lazy_google_maps',
                                    'lazy_ionRangeSlider',
                                    'lazy_tablesorter',
                                    'lazy_KendoUI',
                                    'app/components/plugins/advancedController.js',
                                    'app/features/mytel/cim/cimController.js'
                                    
                                ], { serie: true });
                            }
                        ]
                        ,
                        ts_data: function ($http) {
                            return $http({ method: 'GET', url: 'data/data_cim.json' })
                                .then(function (data) {
                                    return data.data;
                                });
                        }
                    },
                    data: {
                        pageTitle: 'CIM'
                    }
                })

                //2. SAM SALE ACTIVITY MANAGEMENT
                .state("restricted.sale_activity_management", {
                    url: "/",
                    templateUrl: 'app/features/mytel/sam/samSaleActivityManagementView.html',
                    controller: 'samSaleActivityManagementCtrl',
                    resolve: {
                        deps: [
                            '$ocLazyLoad', function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    'lazy_ionRangeSlider',
                                    'lazy_tablesorter',
                                    'lazy_fullcalendar',
                                    'app/features/mytel/sam/samSaleActivityManagementController.js'
                                ], { serie: true });
                            }
                        ]
                        ,
                        ts_data: function ($http) {
                            return $http({ method: 'GET', url: 'data/tablesorter.json' })
                                .then(function (data) {
                                    return data.data;
                                });
                        }
                    },
                    data: {
                        pageTitle: 'SAM SALE ACTIVITY MANAGEMENT'
                    }
                })

                //3. SSM PROPOSAL MANAGEMENT
                .state("restricted.proposal_management", {
                    url: "/",
                    templateUrl: 'app/features/mytel/ssm/ssmProposalManagementView.html',
                    controller: 'ssmProposalManagementCtrl',
                    resolve: {
                        deps: [
                            '$ocLazyLoad', function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    'app/features/mytel/ssm/ssmProposalManagementController.js'
                                ], { serie: true });
                            }
                        ]
                    },
                    data: {
                        pageTitle: 'SSM PROPOSAL MANAGEMENT'
                    }
                })

                //3. SSM PRICE POLICY MANAGEMENT
                .state("restricted.price_policy_management", {
                    url: "/",
                    templateUrl: 'app/features/mytel/ssm/ssmPricePolicyManagementView.html',
                    controller: 'ssmPricePolicyManagementCtrl',
                    resolve: {
                        deps: [
                            '$ocLazyLoad', function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    'lazy_google_maps',
                                    'app/features/mytel/ssm/ssmPricePolicyManagementController.js'
                                ], { serie: true });
                            }
                        ]
                    },
                    data: {
                        pageTitle: 'SSM PRICE POLICY MANAGEMENT'
                    }
                })
                //3. SSM QUOTATION MANAGEMENT
                .state("restricted.quotation_management", {
                    url: "/",
                    templateUrl: 'app/features/mytel/ssm/ssmQuotationManagementView.html',
                    controller: 'ssmQuotationManagementCtrl',
                    resolve: {
                        deps: [
                            '$ocLazyLoad', function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    'app/features/mytel/ssm/ssmQuotationManagementController.js'
                                ], { serie: true });
                            }
                        ]
                    },
                    data: {
                        pageTitle: 'SSM QUOTATION MANAGEMENT'
                    }
                })
                //3. SSM TEMPLATE MANAGEMENT
                .state("restricted.template_management", {
                    url: "/",
                    templateUrl: 'app/features/mytel/ssm/ssmTemplateManagementView.html',
                    controller: 'ssmTemplateManagementCtrl',
                    resolve: {
                        deps: [
                            '$ocLazyLoad', function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    'app/features/mytel/ssm/ssmTemplateManagementController.js'
                                ], { serie: true });
                            }
                        ]
                    },
                    data: {
                        pageTitle: 'SSM TEMPLATE MANAGEMENT'
                    }
                })

                //4. CCM CUSTOMER CARE ACTIVITY
                .state("restricted.customer_care_activity", {
                    url: "/",
                    templateUrl: 'app/features/mytel/ccm/ccmCustomerCareActivityView.html',
                    controller: 'ccmCustomerCareActivityCtrl',
                    resolve: {
                        deps: [
                            '$ocLazyLoad', function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    'lazy_ionRangeSlider',
                                    'lazy_tablesorter',
                                    'lazy_fullcalendar',
                                    'app/features/mytel/ccm/ccmCustomerCareActivityController.js'
                                ], { serie: true });
                            }
                        ],
                        ts_data: function ($http) {
                            return $http({ method: 'GET', url: 'data/tablesorter.json' })
                                .then(function (data) {
                                    return data.data;
                                });
                        }
                    },
                    data: {
                        pageTitle: 'CCM CUSTOMER CARE ACTIVITY'
                    }
                })

                //4. CCM PRESENT LIST
                .state("restricted.present_list", {
                    url: "/",
                    templateUrl: 'app/features/mytel/ccm/ccmPresentListView.html',
                    controller: 'restricted.ccmPresentListCtrl',
                    resolve: {
                        deps: [
                            '$ocLazyLoad', function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    'app/features/mytel/ccm/ccmPresentListController.js'
                                ], { serie: true });
                            }
                        ]
                    },
                    data: {
                        pageTitle: 'CCM PRESENT LIST'
                    }
                })
                //5. DBR HOME PAGE
                .state("restricted.dbrHomePage", {
                    url: "/",
                    templateUrl: 'app/features/mytel/dbr/dbrHomePageView.html',
                    controller: 'dbrHomePageCtrl',
                    resolve: {
                        deps: [
                            '$ocLazyLoad', function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    'lazy_charts_c3',
                                    'lazy_google_maps',
                                    'app/features/mytel/dbr/dbrHomePageController.js'
                                ], { serie: true });
                            }
                        ]
                    },
                    data: {
                        pageTitle: 'DBR Home Page'
                    }
                })

                //5. DBR REPORT
                .state("restricted.dbrReport", {
                    url: "/",
                    templateUrl: 'app/features/mytel/dbr/dbrReportView.html',
                    controller: 'dbrReportCtrl',
                    resolve: {
                        deps: [
                            '$ocLazyLoad', function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    'app/features/mytel/dbr/dbrReportController.js'
                                ], { serie: true });
                            }
                        ]
                    },
                    data: {
                        pageTitle: 'DBR Report'
                    }
                })

        }
    ]);
