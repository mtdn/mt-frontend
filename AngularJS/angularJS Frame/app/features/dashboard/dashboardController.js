/**
 * Created by hienpt on 4/19/17.
 */
angular
    .module('altairApp')
    .controller('dashboardCtrl', [
        '$scope',
        '$interval',
        '$window',
        function ($scope, $interval, $window) {

            // spline chart
            var c3chart_spline_id = 'c3_chart_spline';

            if ($('#' + c3chart_spline_id).length) {

                var c3chart_spline = c3.generate({
                    bindto: '#' + c3chart_spline_id,
                    data: {
                        columns: [
                            ['data1', 30, 200, 100, 400, 150, 250],
                            ['data2', 130, 100, 140, 200, 150, 50]
                        ],
                        type: 'spline'
                    },
                    color: {
                        pattern: ['#5E35B1', '#FB8C00']
                    }
                });

                $($window).on('debouncedresize', c3chart_spline.resize());

                $scope.$on('$destroy', function () {
                    $($window).off('debouncedresize', c3chart_spline.resize());
                    c3chart_spline.destroy();
                });

            }
        }
    ]).controller('gmap_neutralBlueCtrl', [
        '$scope',
        '$timeout',
        function ($scope, $timeout) {
        }
    ]);


