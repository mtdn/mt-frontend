/**
 * Created by QuyenLH1 on 19/06/2018.
 */
angular
    .module('altairApp', [])
    .controller('cimCtrl', [
        '$scope',
        '$rootScope',
        '$timeout',
        '$compile',
        '$http',
        'variables',
        'API_URL',
        function ($scope, $rootScope, $timeout, $compile, $http, variables, API_URL) {

            var $ts_pager_filter, $columnSelector;

            // sQuyenLH1


            // setting data from API

            $scope.customers = [];
            $scope.customerTypes = [];
            $scope.typeOfInvestments = [];
            $scope.typeOfSales = [];

            /**
             *
             * @param filter
             */
            $scope.getCustomers = function (filter) {

                var dataGet = "customers/filters?page=0&size=20&sort=asc&column=customerName&txtSearch="
                    + filter.customerName + "&customerTypeId="
                    + filter.customerTypeId + "&typeOfInvestmentId="
                    + filter.typeOfInvestmentId + "&customerStatus="
                    + filter.customerStatus + "&date=";
                $http.get(API_URL + encodeURI(dataGet))
                    .then(function (response) {
                        if (response.data.meta.code === 200) {
                            $scope.customers = response.data.data.customerList;
                            $scope.itemCustomer.date = response.data.data.date;
                            if ($scope.customers.length === 0) {
                                UIkit.modal.alert("No records found!");
                            }

                            // initialize tables
                            $scope.$on('onLastRepeat', function () {
                                $ts_pager_filter = $("#ts_pager_filter");
                                $columnSelector = $('#columnSelector');

                                var ts_users = $ts_pager_filter
                                    .tablesorter({
                                        theme: 'altair',
                                        widthFixed: true,
                                        widgets: ['zebra', 'filter', 'print', 'columnSelector'],
                                        headers: {
                                            0: {
                                                sorter: false,
                                                parser: false
                                            }
                                        },
                                        widgetOptions: {
                                            // column selector widget
                                            columnSelector_container: $columnSelector,
                                            columnSelector_name: 'data-name',
                                            columnSelector_layout: '<li class="padding_md"><input type="checkbox"><label class="inline-label">{name}</label></li>',
                                            columnSelector_saveColumns: false,
                                            // print widget
                                            print_title: '',          // this option > caption > table id > "table"
                                            print_dataAttrib: 'data-name', // header attrib containing modified header name
                                            print_rows: 'f',         // (a)ll, (v)isible, (f)iltered, or custom css selector
                                            print_columns: 's',         // (a)ll, (v)isible or (s)elected (columnSelector widget)
                                            print_extraCSS: '',          // add any extra css definitions for the popup window here
                                            print_styleSheet: '',          // add the url of your print stylesheet
                                            print_now: true,        // Open the print dialog immediately if true
                                            // callback executed when processing completes - default setting is null
                                            print_callback: function (config, $table, printStyle) {
                                                // hide sidebar
                                                $rootScope.primarySidebarActive = false;
                                                $rootScope.primarySidebarOpen = false;
                                                $timeout(function () {
                                                    // print the table using the following code
                                                    $.tablesorter.printTable.printOutput(config, $table.html(), printStyle);
                                                }, 300);
                                            }
                                        }
                                    })
                                    .on('pagerComplete', function (e, filter) {
                                        // update selectize value
                                        if (typeof selectizeObj !== 'undefined' && selectizeObj.data('selectize')) {
                                            selectizePage = selectizeObj[0].selectize;
                                            selectizePage.setValue($('select.ts_gotoPage option:selected').index() + 1, false);
                                        }
                                    });

                                // replace column selector checkboxes
                                $columnSelector.children('li').each(function (index) {
                                    var $this = $(this);

                                    var id = index == 0 ? 'auto' : index;
                                    $this.children('input').attr('id', 'column_' + id);
                                    $this.children('label').attr('for', 'column_' + id);

                                    $this.children('input')
                                        .prop('checked', true)
                                        .iCheck({
                                            checkboxClass: 'icheckbox_md',
                                            radioClass: 'iradio_md',
                                            increaseArea: '20%'
                                        });

                                    if (index != 0) {
                                        $this.find('input')
                                            .on('ifChanged', function (ev) {
                                                $(ev.target).toggleClass('checked').change();
                                            })
                                    }

                                });

                                $('#column_auto')
                                    .on('ifChecked', function (ev) {
                                        $(this)
                                            .closest('li')
                                            .siblings('li')
                                            .find('input').iCheck('disable');
                                        $(ev.target).removeClass('checked').change();
                                    })
                                    .on('ifUnchecked', function (ev) {
                                        $(this)
                                            .closest('li')
                                            .siblings('li')
                                            .find('input').iCheck('enable');
                                        $(ev.target).addClass('checked').change();
                                    });

                                // select/unselect table rows
                                $('.ts_checkbox_all')
                                    .iCheck({
                                        checkboxClass: 'icheckbox_md',
                                        radioClass: 'iradio_md',
                                        increaseArea: '20%'
                                    })
                                    .on('ifChecked', function () {
                                        $ts_pager_filter
                                            .find('.ts_checkbox')
                                            // check all checkboxes in table
                                            .prop('checked', true)
                                            .iCheck('update')
                                            // add highlight to row
                                            .closest('tr')
                                            .addClass('row_highlighted');
                                    })
                                    .on('ifUnchecked', function () {
                                        $ts_pager_filter
                                            .find('.ts_checkbox')
                                            // uncheck all checkboxes in table
                                            .prop('checked', false)
                                            .iCheck('update')
                                            // remove highlight from row
                                            .closest('tr')
                                            .removeClass('row_highlighted');
                                    });

                                // select/unselect table row
                                $ts_pager_filter.find('.ts_checkbox')
                                    .on('ifUnchecked', function () {
                                        $(this).closest('tr').removeClass('row_highlighted');
                                        $('.ts_checkbox_all').prop('checked', false).iCheck('update');
                                    }).on('ifChecked', function () {
                                        $(this).closest('tr').addClass('row_highlighted');
                                    });

                                ts_users.trigger('update');

                            });
                        }
                    }, function responseError() {
                        UIkit.modal.alert("get Customers error!");
                    });
            };

            /**
             * init
             */
            $scope.init = function () {
                $scope.getCustomers({
                    customerName: "",
                    customerTypeId: 0,
                    typeOfInvestmentId: 0,
                    customerStatus: 3
                });
                $http.get(API_URL + "customer-types")
                    .then(function (response) {
                        if (response.data.meta.code === 200) {
                            $scope.customerTypes = response.data.data.customerTypes;
                            console.log($scope.customerTypes);
                        }
                    }, function responseError() {
                        UIkit.modal.alert("get customerTypes error!");
                    });
                $http.get(API_URL + "customer-classifications?type=investment")
                    .then(function (response) {
                        if (response.data.meta.code === 200) {
                            $scope.typeOfInvestments = response.data.data.customerClassifications;
                            console.log($scope.typeOfInvestments);
                        }
                    }, function responseError() {
                        UIkit.modal.alert("get typeOfInvestments error!");
                    });
                $http.get(API_URL + "customer-classifications?type=sale")
                    .then(function (response) {
                        if (response.data.meta.code === 200) {
                            $scope.typeOfSales = response.data.data.customerClassifications;
                            console.log($scope.typeOfSales);
                        }
                    }, function responseError() {
                        UIkit.modal.alert("get typeOfSales error!");
                    });

            };
            $scope.init();

            /**
             * removeCustomer
             * @param index
             */
            $scope.removeCustomer = function (index) {
                UIkit.modal.confirm('Are you sure you want to delete this user?', function () {
                    $http.delete(API_URL + "customers/" + $scope.customers[index].id)
                        .then(function (response) {
                            $scope.searchCustomer();
                        }, function responseError() {
                            UIkit.modal.alert("delete customers error!");
                        });
                }, {
                    labels: {
                        'Ok': 'OK'
                    }
                });
            };

            $scope.itemEdit = {};
            var idxCustomer = -1;

            /**
             * editCustomer
             * @type {number}
             */
            $scope.editCustomer = function (index) {
                idxCustomer = index;
                $scope.itemEdit = angular.copy($scope.customers[idxCustomer]);
                $scope.openModal('popupEdit');
            };

            /**
             * saveEditCustomer
             */
            $scope.saveEditCustomer = function () {
                $scope.customers[idxCustomer].customerName = $scope.itemEdit.customerName;
                $scope.customers[idxCustomer].customerTypeId = $scope.itemEdit.customerTypeId;
                $scope.customers[idxCustomer].typeOfInvestmentId = $scope.itemEdit.typeOfInvestmentId;
                $scope.customers[idxCustomer].typeOfSaleId = $scope.itemEdit.typeOfSaleId;
                $scope.customers[idxCustomer].customerStatus = $scope.itemEdit.customerStatus;
                $scope.customers[idxCustomer].address = $scope.itemEdit.address;
                $scope.customers[idxCustomer].catalog = $scope.itemEdit.catalog;
                $scope.customers[idxCustomer].contactName = $scope.itemEdit.contactName;
                $scope.customers[idxCustomer].phone = $scope.itemEdit.phone;
                $scope.customers[idxCustomer].email = $scope.itemEdit.email;
                $scope.customers[idxCustomer].position = $scope.itemEdit.position;
                $scope.customers[idxCustomer].service = $scope.itemEdit.service;
                $scope.customers[idxCustomer].customerDate = $scope.itemEdit.customerDate;
                $scope.itemEdit = {};
                $http.put(API_URL + "customers/" + $scope.customers[idxCustomer].id, $scope.customers[idxCustomer])
                    .then(function (response) {
                        if (response.data.meta.code === 200) {
                            $scope.closeModal('popupEdit');
                        }
                    }, function responseError() {
                        UIkit.modal.alert("put customers error!");
                    });
            };

            /**
             * openModal
             * @param idName
             */
            $scope.openModal = function (idName) {
                document.getElementById(idName).style.display = "block";
            };

            /**
             * closeModal
             * @param idName
             */
            $scope.closeModal = function (idName) {
                document.getElementById(idName).style.display = "none";
            };

            // add new customer
            $scope.itemCreate = {
                customerTypeId: 1,
                typeOfSaleId: 3,
                typeOfInvestmentId: 1,
                customerStatus: 0,
                customerDate: getDate()
            };
            $scope.itemCreateClone = angular.copy($scope.itemCreate);

            /**
             *
             * @returns {yyyy-MM-dd}
             */
            function getDate() {
                var today = new Date();
                var dd = today.getDate();
                var MM = today.getMonth() + 1; //January is 0!
                var yyyy = today.getFullYear();
                if (dd < 10) {
                    dd = '0' + dd;
                }
                if (MM < 10) {
                    MM = '0' + MM;
                }
                return yyyy + "-" + MM + "-" + dd;
            }

            /**
             * createCustomer
             */
            $scope.createCustomer = function () {
                var itemCreate = angular.copy($scope.itemCreate);
                $http.post(API_URL + "customers", itemCreate)
                    .then(function (response) {
                        if (response.data.meta.code === 200) {
                            $scope.customers.push(itemCreate);
                            $scope.itemCreate = angular.copy($scope.itemCreateClone);
                            UIkit.modal.alert(response.data.meta.message);
                        }
                    }, function responseError() {
                        UIkit.modal.alert("post customers error!");
                    });
            };

            //search customer
            $scope.itemCustomer = {
                customerName: "",
                customerTypeId: 1,
                typeOfInvestmentId: 1,
                customerStatus: 0
            };

            var isFirst = true;
            /**
             * searchCustomer
             */
            $scope.searchCustomer = function () {
                var itemSearch = angular.copy($scope.itemCustomer);
                $scope.getCustomers(itemSearch);
                isFirst = false;
            };

            /**
             *
             */
            $scope.exportCustomer = function () {
                if (isFirst) { //export all data in the first
                    $scope.itemCustomer.customerTypeId = 0;
                    $scope.itemCustomer.typeOfInvestmentId = 0;
                    $scope.itemCustomer.customerStatus = 3;
                }
                if ($scope.itemCustomer.date !== undefined) {
                    var dataExport = "page=0&size=20&sort=asc&column=customerName"
                        + "&txtSearch=" + $scope.itemCustomer.customerName
                        + "&customerTypeId=" + $scope.itemCustomer.customerTypeId
                        + "&typeOfInvestmentId=" + $scope.itemCustomer.typeOfInvestmentId
                        + "&customerStatus=" + $scope.itemCustomer.customerStatus
                        + "&date=" + $scope.itemCustomer.date;
                    var anchor = angular.element(document.querySelector('#export'));
                    anchor.attr({
                        href: API_URL + "customers/export?" + encodeURI(dataExport)
                    });
                }
            };

            /**
             *
             * @param files
             */
            $scope.uploadFile = function (files) {
                var fd = new FormData();
                fd.append("file", files[0]);

                $http.post(API_URL + "customers/import", fd, {
                    withCredentials: true,
                    headers: {'Content-Type': undefined},
                    transformRequest: angular.identity
                }).success(UIkit.modal.alert("OK!")).error();

            };

            // eQuyenLH1
        }
    ])
    .controller('gmap_neutralBlueCtrl', [
        '$scope',
        '$timeout',
        function ($scope, $timeout) {
        }
    ]).constant(
    'API_URL',
    // 'http://203.81.74.34:8001/api/',
    'http://203.81.74.34:8000/wsapi/api/'
);