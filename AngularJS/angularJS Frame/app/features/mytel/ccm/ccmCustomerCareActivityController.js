/**
 * Created by hienpt on 4/19/17.
 */
angular
    .module('altairApp')
    .controller('ccmCustomerCareActivityCtrl', [
        '$scope',
        '$rootScope',
        '$timeout',
        '$compile',
        'variables',
        'ts_data',
        function ($scope, $rootScope, $timeout, $compile, variables, ts_data) {

            $scope.table_data = ts_data;

            $scope.alignChar_table_data = [
                ["abc 123", ".423475", "Koala = cute = cudley", "search.google.com"],
                ["abc 1", "23.4", "Ox = stinky", "mail.yahoo.com"],
                ["bc 9", "1.0", "Girafee = tall", "http://www.facebook.com"],
                ["zyx 24", "7.67", "Bison = burger", "http://internship.whitehouse.gov/"],
                ["abc 11", "3000", "Chimp = banana lover", "about.ucla.edu"],
                ["abc 2", "56.5", "Elephant = unforgetable", "http://www.wikipedia.org/"],
                ["abc 9", "15.5", "Lion = rawr", "rental.nytimes.com/index.html"],
                ["ABC 10", "87.20000", "Zebra = stripey", "http://android.google.com"],
                ["zyx 1", "999.1", "Koala = cute, again!", "http://irsmrt.mit.edu/"],
                ["zyx 12", "0.2", "Llama = llove it", "http://aliens.nasa.gov/"]
            ];

            $scope.customFilters_table_data = [
                ['abc 123', '10', 'Koala', 'Mar 5, 2014'],
                ['abc 1', '234', 'Ox', 'Jan 2, 2014'],
                ['abc 9', '10', 'Girafee', 'Sep 20, 2013'],
                ['zyx 24', '767', 'Bison', 'Apr 8, 2014'],
                ['abc 11', '3', 'Chimp', 'Feb 5, 2014'],
                ['abc 2', '56', 'Elephant', 'Dec 18, 2013'],
                ['abc 9', '155', 'Lion', 'Mar 18, 2014'],
                ['ABC 10', '87', 'Zebra', 'Jul 1, 2014'],
                ['zyx 1', '999', 'Koala', 'Jan 18, 2014'],
                ['zyx 12', '0', 'Llama', 'Jan 20, 2015']
            ];

            // initialize tables
            $scope.$on('onLastRepeat', function (scope, element, attrs) {

                var $ts_pager_filter = $("#ts_pager_filter"),
                    $ts_align = $('#ts_align'),
                    $ts_customFilters = $('#ts_custom_filters'),
                    $columnSelector = $('#columnSelector');

                // pager + filter
                if ($(element).closest($ts_pager_filter).length) {

                    // define pager options
                    var pagerOptions = {
                        // target the pager markup - see the HTML block below
                        container: $(".ts_pager"),
                        // output string - default is '{page}/{totalPages}'; possible variables: {page}, {totalPages}, {startRow}, {endRow} and {totalRows}
                        output: '{startRow} - {endRow} / {filteredRows} ({totalRows})',
                        // if true, the table will remain the same height no matter how many records are displayed. The space is made up by an empty
                        // table row set to a height to compensate; default is false
                        fixedHeight: true,
                        // remove rows from the table to speed up the sort of large tables.
                        // setting this to false, only hides the non-visible rows; needed if you plan to add/remove rows with the pager enabled.
                        removeRows: false,
                        // go to page selector - select dropdown that sets the current page
                        cssGoto: '.ts_gotoPage'
                    };

                    // change popup print & close button text
                    $.tablesorter.language.button_print = "Print table";
                    $.tablesorter.language.button_close = "Cancel";

                    // print table
                    $('#printTable').on('click', function (e) {
                        e.preventDefault();
                        $ts_pager_filter.trigger('printTable');
                    });

                    // Initialize tablesorter
                    var ts_users = $ts_pager_filter
                        .tablesorter({
                            theme: 'altair',
                            widthFixed: true,
                            widgets: ['zebra', 'filter', 'print', 'columnSelector'],
                            headers: {
                                0: {
                                    sorter: false,
                                    parser: false
                                }
                            },
                            widgetOptions: {
                                // column selector widget
                                columnSelector_container: $columnSelector,
                                columnSelector_name: 'data-name',
                                columnSelector_layout: '<li class="padding_md"><input type="checkbox"><label class="inline-label">{name}</label></li>',
                                columnSelector_saveColumns: false,
                                // print widget
                                print_title: '',          // this option > caption > table id > "table"
                                print_dataAttrib: 'data-name', // header attrib containing modified header name
                                print_rows: 'f',         // (a)ll, (v)isible, (f)iltered, or custom css selector
                                print_columns: 's',         // (a)ll, (v)isible or (s)elected (columnSelector widget)
                                print_extraCSS: '',          // add any extra css definitions for the popup window here
                                print_styleSheet: '',          // add the url of your print stylesheet
                                print_now: true,        // Open the print dialog immediately if true
                                // callback executed when processing completes - default setting is null
                                print_callback: function (config, $table, printStyle) {
                                    // hide sidebar
                                    $rootScope.primarySidebarActive = false;
                                    $rootScope.primarySidebarOpen = false;
                                    $timeout(function () {
                                        // print the table using the following code
                                        $.tablesorter.printTable.printOutput(config, $table.html(), printStyle);
                                    }, 300);
                                }
                            }
                        })
                        // initialize the pager plugin
                        .tablesorterPager(pagerOptions)
                        .on('pagerComplete', function (e, filter) {
                            // update selectize value
                            if (typeof selectizeObj !== 'undefined' && selectizeObj.data('selectize')) {
                                selectizePage = selectizeObj[0].selectize;
                                selectizePage.setValue($('select.ts_gotoPage option:selected').index() + 1, false);
                            }
                        });

                    // replace column selector checkboxes
                    $columnSelector.children('li').each(function (index) {
                        var $this = $(this);

                        var id = index == 0 ? 'auto' : index;
                        $this.children('input').attr('id', 'column_' + id);
                        $this.children('label').attr('for', 'column_' + id);

                        $this.children('input')
                            .prop('checked', true)
                            .iCheck({
                                checkboxClass: 'icheckbox_md',
                                radioClass: 'iradio_md',
                                increaseArea: '20%'
                            });

                        if (index != 0) {
                            $this.find('input')
                                .on('ifChanged', function (ev) {
                                    $(ev.target).toggleClass('checked').change();
                                })
                        }

                    });

                    $('#column_auto')
                        .on('ifChecked', function (ev) {
                            $(this)
                                .closest('li')
                                .siblings('li')
                                .find('input').iCheck('disable');
                            $(ev.target).removeClass('checked').change();
                        })
                        .on('ifUnchecked', function (ev) {
                            $(this)
                                .closest('li')
                                .siblings('li')
                                .find('input').iCheck('enable');
                            $(ev.target).addClass('checked').change();
                        });

                    // replace 'goto Page' select
                    function createPageSelectize() {
                        selectizeObj = $('select.ts_gotoPage')
                            .val($("select.ts_gotoPage option:selected").val())
                            .after('<div class="selectize_fix"></div>')
                            .selectize({
                                hideSelected: true,
                                onDropdownOpen: function ($dropdown) {
                                    $dropdown
                                        .hide()
                                        .velocity('slideDown', {
                                            duration: 200,
                                            easing: variables.easing_swiftOut
                                        })
                                },
                                onDropdownClose: function ($dropdown) {
                                    $dropdown
                                        .show()
                                        .velocity('slideUp', {
                                            duration: 200,
                                            easing: variables.easing_swiftOut
                                        });

                                    // hide tooltip
                                    $('.uk-tooltip').hide();
                                }
                            });
                    }
                    createPageSelectize();

                    // replace 'pagesize' select
                    $('.pagesize.ts_selectize')
                        .after('<div class="selectize_fix"></div>')
                        .selectize({
                            hideSelected: true,
                            onDropdownOpen: function ($dropdown) {
                                $dropdown
                                    .hide()
                                    .velocity('slideDown', {
                                        duration: 200,
                                        easing: variables.easing_swiftOut
                                    })
                            },
                            onDropdownClose: function ($dropdown) {
                                $dropdown
                                    .show()
                                    .velocity('slideUp', {
                                        duration: 200,
                                        easing: variables.easing_swiftOut
                                    });

                                // hide tooltip
                                $('.uk-tooltip').hide();

                                if (typeof selectizeObj !== 'undefined' && selectizeObj.data('selectize')) {
                                    selectizePage = selectizeObj[0].selectize;
                                    selectizePage.destroy();
                                    $('.ts_gotoPage').next('.selectize_fix').remove();
                                    setTimeout(function () {
                                        createPageSelectize()
                                    })
                                }

                            }
                        });

                    // select/unselect table rows
                    $('.ts_checkbox_all')
                        .iCheck({
                            checkboxClass: 'icheckbox_md',
                            radioClass: 'iradio_md',
                            increaseArea: '20%'
                        })
                        .on('ifChecked', function () {
                            $ts_pager_filter
                                .find('.ts_checkbox')
                                // check all checkboxes in table
                                .prop('checked', true)
                                .iCheck('update')
                                // add highlight to row
                                .closest('tr')
                                .addClass('row_highlighted');
                        })
                        .on('ifUnchecked', function () {
                            $ts_pager_filter
                                .find('.ts_checkbox')
                                // uncheck all checkboxes in table
                                .prop('checked', false)
                                .iCheck('update')
                                // remove highlight from row
                                .closest('tr')
                                .removeClass('row_highlighted');
                        });

                    // select/unselect table row
                    $ts_pager_filter.find('.ts_checkbox')
                        .on('ifUnchecked', function () {
                            $(this).closest('tr').removeClass('row_highlighted');
                            $('.ts_checkbox_all').prop('checked', false).iCheck('update');
                        }).on('ifChecked', function () {
                            $(this).closest('tr').addClass('row_highlighted');
                        });

                    // remove single row
                    $ts_pager_filter.on('click', '.ts_remove_row', function (e) {
                        e.preventDefault();

                        var $this = $(this);
                        UIkit.modal.confirm('Are you sure you want to delete this user?', function () {
                            $this.closest('tr').remove();
                            ts_users.trigger('update');
                        }, {
                            labels: {
                                'Ok': 'Delete'
                            }
                        });
                    });
                }

                // align widget example
                if ($(element).closest($ts_align).length) {
                    $ts_align.tablesorter({
                        theme: 'altair',
                        widgets: ['zebra', 'alignChar'],
                        widgetOptions: {
                            alignChar_wrap: '<i/>',
                            alignChar_charAttrib: 'data-align-char',
                            alignChar_indexAttrib: 'data-align-index',
                            alignChar_adjustAttrib: 'data-align-adjust' // percentage width adjustments
                        }
                    });
                }

                // custom filters
                if ($(element).closest($ts_customFilters).length) {
                    $ts_customFilters
                        .tablesorter({
                            theme: 'altair',
                            headerTemplate: '{content} {icon}',
                            widgets: ['zebra', 'filter'],
                            widgetOptions: {
                                filter_reset: 'button.ts_cf_reset',
                                filter_cssFilter: ['', 'ts_cf_range', 'ts_cf_select_single', 'ts_cf_datepicker']
                            }
                        })
                        .on('apply.daterangepicker', function () {
                            $table.trigger('search');
                        });

                    // rangeSlider
                    var slider = $('.ts_cf_range').ionRangeSlider({
                        "min": "0",
                        "max": "1000",
                        "type": "double",
                        "grid-num": "10",
                        "from-min": "10",
                        "from-max": "30",
                        "input_values_separator": " - "
                    }).data("ionRangeSlider");

                    // selectize
                    var $selectize = $('.ts_cf_select_single')
                        .after('<div class="selectize_fix"></div>')
                        .selectize({
                            hideSelected: true,
                            dropdownParent: 'body',
                            closeAfterSelect: true,
                            onDropdownOpen: function ($dropdown) {
                                $dropdown
                                    .hide()
                                    .velocity('slideDown', {
                                        duration: 200,
                                        easing: [0.4, 0, 0.2, 1]
                                    })
                            },
                            onDropdownClose: function ($dropdown) {
                                $dropdown
                                    .show()
                                    .velocity('slideUp', {
                                        duration: 200,
                                        easing: [0.4, 0, 0.2, 1]
                                    });
                            }
                        });

                    var cf_selectize = $selectize[0].selectize;

                    var modal = UIkit.modal("#modal_daterange", {
                        center: true
                    });

                    $('.ts_cf_datepicker').on('focus', function () {
                        if (modal.isActive()) {
                            modal.hide();
                        } else {
                            modal.show();
                        }
                    });

                    var $dp_start = $('#ts_dp_start'),
                        $dp_end = $('#ts_dp_end');

                    var start_date = UIkit.datepicker($dp_start, {
                        format: 'MMM D, YYYY'
                    });

                    var end_date = UIkit.datepicker($dp_end, {
                        format: 'MMM D, YYYY'
                    });

                    $dp_start.on('change', function () {
                        end_date.options.minDate = $dp_start.val();
                    });

                    $dp_end.on('change', function () {
                        start_date.options.maxDate = $dp_end.val();
                    });

                    $('#daterangeApply').on('click', function (e) {
                        e.preventDefault();
                        modal.hide();
                        $('.ts_cf_datepicker').val(
                            $dp_start.val() + ' - ' + $dp_end.val()
                        ).change().blur();
                    });

                    $('button.ts_cf_reset').on('click', function () {
                        // reset selectize
                        cf_selectize.clear();
                        // slider reset
                        slider.reset();
                    })

                }

            });

            // model change
            $timeout(function () {
                $scope.forms_advanced.datepicker = "23.06.2016"
            }, 5000);

            // date range
            var $dp_start = $('#uk_dp_start'),
                $dp_end = $('#uk_dp_end');

            var start_date = UIkit.datepicker($dp_start, {
                format: 'DD.MM.YYYY'
            });

            var end_date = UIkit.datepicker($dp_end, {
                format: 'DD.MM.YYYY'
            });

            $dp_start.on('change', function () {
                end_date.options.minDate = $dp_start.val();
            });

            $dp_end.on('change', function () {
                start_date.options.maxDate = $dp_end.val();
            });
        }
    ]).controller('calendarCtrl', [
        '$scope',
        'uiCalendarConfig',
        function ($scope, uiCalendarConfig) {

            $scope.randID_generator = function () {
                var randLetter = String.fromCharCode(65 + Math.floor(Math.random() * 26));
                return randLetter + Date.now();
            };

            $scope.color_picker = function (object, pallete) {
                if (object) {
                    var cp_id = $scope.randID_generator(),
                        cp_pallete = pallete ? pallete : ['#e53935', '#d81b60', '#8e24aa', '#5e35b1', '#3949ab', '#1e88e5', '#039be5', '#0097a7', '#00897b', '#43a047', '#689f38', '#ef6c00', '#f4511e', '#6d4c41', '#757575', '#546e7a'],
                        cp_pallete_length = cp_pallete.length,
                        cp_wrapper = $('<div class="cp_altair" id="' + cp_id + '"/>');

                    for (var $i = 0; $i < cp_pallete_length; $i++) {
                        cp_wrapper.append('<span data-color=' + cp_pallete[$i] + ' style="background:' + cp_pallete[$i] + '"></span>');
                    }

                    cp_wrapper.append('<input type="hidden">');

                    $('body').on('click', '#' + cp_id + ' span', function () {
                        $(this)
                            .addClass('active_color')
                            .siblings().removeClass('active_color')
                            .end()
                            .closest('.cp_altair').find('input').val($(this).attr('data-color'));
                    });
                    return object.append(cp_wrapper);

                }
            };

            $scope.calendarColorPicker = $scope.color_picker($('<div id="calendar_colors_wrapper"></div>')).prop('outerHTML');

            $scope.uiConfig = {
                calendar: {
                    header: {
                        left: 'title today',
                        center: '',
                        right: 'month,agendaWeek,agendaDay,listWeek prev,next'
                    },
                    buttonIcons: {
                        prev: 'md-left-single-arrow',
                        next: 'md-right-single-arrow',
                        prevYear: 'md-left-double-arrow',
                        nextYear: 'md-right-double-arrow'
                    },
                    buttonText: {
                        today: ' ',
                        month: ' ',
                        week: ' ',
                        day: ' '
                    },
                    aspectRatio: 2.1,
                    defaultDate: moment(),
                    selectable: true,
                    selectHelper: true,
                    select: function (start, end) {
                        UIkit.modal.prompt('' +
                            '<h3 class="heading_b uk-margin-medium-bottom">New Event</h3><div class="uk-margin-medium-bottom" id="calendar_colors">' +
                            'Event Color:' +
                            $scope.calendarColorPicker +
                            '</div>' +
                            'Event Title:',
                            '', function (newvalue) {
                                if ($.trim(newvalue) !== '') {
                                    var eventData,
                                        eventColor = $('#calendar_colors_wrapper').find('input').val();
                                    eventData = {
                                        title: newvalue,
                                        start: start,
                                        end: end,
                                        color: eventColor ? eventColor : ''
                                    };
                                    uiCalendarConfig.calendars.myCalendar.fullCalendar('renderEvent', eventData, true); // stick? = true
                                    uiCalendarConfig.calendars.myCalendar.fullCalendar('unselect');
                                }
                            }, {
                                labels: {
                                    Ok: 'Add Event'
                                }
                            });
                    },
                    editable: true,
                    eventLimit: true,
                    timeFormat: '(HH)(:mm)'
                }
            };

            $scope.calendar_events = [
                {
                    title: 'All Day Event',
                    start: moment().startOf('month').add(25, 'days').format('YYYY-MM-DD')
                },
                {
                    title: 'Long Event',
                    start: moment().startOf('month').add(3, 'days').format('YYYY-MM-DD'),
                    end: moment().startOf('month').add(7, 'days').format('YYYY-MM-DD')
                },
                {
                    id: 999,
                    title: 'Repeating Event',
                    start: moment().startOf('month').add(8, 'days').format('YYYY-MM-DD'),
                    color: '#689f38'
                },
                {
                    id: 999,
                    title: 'Repeating Event',
                    start: moment().startOf('month').add(15, 'days').format('YYYY-MM-DD'),
                    color: '#689f38'
                },
                {
                    title: 'Conference',
                    start: moment().startOf('day').add(14, 'hours').format('YYYY-MM-DD HH:mm'),
                    end: moment().startOf('day').add(15, 'hours').format('YYYY-MM-DD HH:mm')
                },
                {
                    title: 'Meeting',
                    start: moment().startOf('month').add(14, 'days').add(10, 'hours').format('YYYY-MM-DD HH:mm'),
                    color: '#7b1fa2'
                },
                {
                    title: 'Lunch',
                    start: moment().startOf('day').add(11, 'hours').format('YYYY-MM-DD HH:mm'),
                    color: '#d84315'
                },
                {
                    title: 'Meeting',
                    start: moment().startOf('day').add(8, 'hours').format('YYYY-MM-DD HH:mm'),
                    color: '#7b1fa2'
                },
                {
                    title: 'Happy Hour',
                    start: moment().startOf('month').add(1, 'days').format('YYYY-MM-DD')
                },
                {
                    title: 'Dinner',
                    start: moment().startOf('day').add(19, 'hours').format('YYYY-MM-DD HH:mm')
                },
                {
                    title: 'Birthday Party',
                    start: moment().startOf('month').add(23, 'days').format('YYYY-MM-DD')
                },
                {
                    title: 'NEW RELEASE (link)',
                    url: 'http://google.com',
                    start: moment().startOf('month').add(10, 'days').format('YYYY-MM-DD'),
                    color: '#0097a7'
                }
            ];

            $scope.eventSources = [$scope.calendar_events];

        }
    ])