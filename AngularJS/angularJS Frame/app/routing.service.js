/**
 * Created by hienpt on 1/12/17.
 */
altairApp
    .service('routing', [
        '$state',
        function($state) {
            var vm = this;
            //start sample

           vm.gotoDashboard = function () {
                 $state.go('restricted.dashboard');
            };
            vm.gotoConfirmUserPage = function (edituser) {
                $state.go('restricted.createuserconfirmpage', {
                    edituser: edituser,
                });
            };
            //end sample
        }
    ])
;